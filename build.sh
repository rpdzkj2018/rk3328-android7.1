#!/bin/bash

# build args
ARCH=arm64
LUNCH=rk3328_box-userdebug
UBOOT_DEFCONFIG=rk3328_box_defconfig
KERNEL_DEFCONFIG=rockchip_smp_nougat_defconfig
KERNEL_DTS=rp-rk3328
JOBS=32

source build/envsetup.sh >/dev/null && setpaths
lunch $LUNCH
TARGET_PRODUCT=`get_build_var TARGET_PRODUCT`

#set jdk version
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export PATH=$JAVA_HOME/bin:$PATH
export CLASSPATH=.:$JAVA_HOME/lib:$JAVA_HOME/lib/tools.jar
# source environment and chose target product
DEVICE=`get_build_var TARGET_PRODUCT`
BUILD_VARIANT=`get_build_var TARGET_BUILD_VARIANT`
PACK_TOOL_DIR=RKTools/linux/Linux_Pack_Firmware
IMAGE_PATH=rockdev/Image-$TARGET_PRODUCT
export PROJECT_TOP=`gettop`

#lunch $DEVICE-$BUILD_VARIANT

PLATFORM_VERSION=`get_build_var PLATFORM_VERSION`
DATE=$(date  +%Y%m%d.%H%M)
STUB_PATH=Image/"$KERNEL_DTS"_"$PLATFORM_VERSION"_"$DATE"_RELEASE_TEST
STUB_PATH="$(echo $STUB_PATH | tr '[:lower:]' '[:upper:]')"
export STUB_PATH=$PROJECT_TOP/$STUB_PATH
export STUB_PATCH_PATH=$STUB_PATH/PATCHES


function build_uboot {
	# build uboot
	echo "start build uboot"

	cd u-boot && make ARCHV=aarch64 -j$JOBS && cd -
	if [ $? -eq 0 ]; then
		echo "Build uboot ok!"
	else
		echo "Build uboot failed!"
		exit 1
	fi
}


function build_kernel {
	# build kernel
	echo "Start build kernel"
	cd kernel &&  make ARCH=arm64 $KERNEL_DTS.img -j$JOBS && cd -
	if [ $? -eq 0 ]; then
		echo "Build kernel ok!"
	else
		echo "Build kernel failed!"
		exit 1
	fi
	# build wifi ko
	#source device/rockchip/common/build_wifi_ko.sh
}

function build_android {
	# build android
	echo "start build android"
	#make installclean
	make -j$JOBS
	if [ $? -eq 0 ]; then
		echo "Build android ok!"
	else
		echo "Build android failed!"
		exit 1
	fi
}

function build_mkimage {
	# mkimage.sh
	echo "make and copy android images"
	./mkimage.sh
	if [ $? -eq 0 ]; then
		echo "Make image ok!"
	else
		echo "Make image failed!"
		exit 1
	fi
	
	
	mkdir -p $PACK_TOOL_DIR/rockdev/Image/
	cp -f $IMAGE_PATH/* $PACK_TOOL_DIR/rockdev/Image/

	echo "Make update.img"
	cd $PACK_TOOL_DIR/rockdev && ./mkupdate.sh
	if [ $? -eq 0 ]; then
		echo "Make update image ok!"
	else
		echo "Make update image failed!"
		exit 1
	fi
	cd -

	mv $PACK_TOOL_DIR/rockdev/update.img $IMAGE_PATH/
	rm $PACK_TOOL_DIR/rockdev/Image -rf
}


function build_ota {
    INTERNAL_OTA_PACKAGE_OBJ_TARGET=obj/PACKAGING/target_files_intermediates/$TARGET_PRODUCT-target_files-*.zip
    INTERNAL_OTA_PACKAGE_TARGET=$TARGET_PRODUCT-ota-*.zip
    echo "generate ota package"
    make otapackage -j$JOBS
    ./mkimage.sh ota
    cp $OUT/$INTERNAL_OTA_PACKAGE_TARGET $IMAGE_PATH/
    cp $OUT/$INTERNAL_OTA_PACKAGE_OBJ_TARGET $IMAGE_PATH/
}


function build_save {
	mkdir -p $STUB_PATH
	mkdir -p $STUB_PATCH_PATH/kernel
	cp kernel/.config $STUB_PATCH_PATH/kernel
	cp kernel/vmlinux $STUB_PATCH_PATH/kernel

	mkdir -p $STUB_PATH/IMAGES/
	cp $IMAGE_PATH/* $STUB_PATH/IMAGES/
	#Save build command info
	echo "UBOOT:  defconfig: $UBOOT_DEFCONFIG" >> $STUB_PATH/build_cmd_info
	echo "KERNEL: defconfig: $KERNEL_DEFCONFIG, dts: $KERNEL_DTS" >> $STUB_PATH/build_cmd_info
	echo "ANDROID:$DEVICE-$BUILD_VARIANT" >> $STUB_PATH/build_cmd_info
}

function build_all {
	build_uboot
	build_kernel
	build_android
	build_mkimage
}


OPTIONS="$@"
for option in ${OPTIONS:-all}; do
        echo "processing option: $option"
        case $option in
                *)
                        eval build_$option
                        ;;
        esac
done
